using UnityEngine;
using System.Collections.Generic;
using TransTech.System.Markup;
using TransTech.System.Debug;
using UnityExtensions;

public class BookController : MonoBehaviour 
{
    private class PageComponents
    {
        public PageController Controller { get; private set; }
        public PageRenderer FrontPage { get; private set; }
        public PageRenderer BackPage { get; private set; }

        public PageComponents(PageController controller, PageRenderer frontPage, PageRenderer backPage)
        {
            Controller = controller;
            FrontPage = frontPage;
            BackPage = backPage;
        }
    }

    private MarkupParser m_Parser;

    public UIFont m_NormalFont;
    public UIFont m_HeadingFont;
    public UIFont m_BoldFont;
    public UIFont m_ItalicFont;

    public GameObject m_LabelPrefab;

    public TextAsset[] m_Chapters;
    private int m_CurrentChapter;

    // Outer Dictionary key is chapter name, value is index lookup values for that chapter
    // Inner Dictionary Key is a Word Location, value is the character location
    private Dictionary<string, Dictionary<int, int>> m_WordPosLookup = new Dictionary<string, Dictionary<int, int>>();

    public GameObject m_PageRendererPrefab;
    public GameObject m_PageControllerPrefab;

    private List<PageRenderer> m_FrontPageRenderers = new List<PageRenderer>();
    private List<PageRenderer> m_BackPageRenderers = new List<PageRenderer>();

    public List<PageController> m_Pages = new List<PageController>();

    private PageComponents m_TurningPage;

    private List<PageComponents> m_LeftPages = new List<PageComponents>();
    private List<PageComponents> m_RightPages = new List<PageComponents>();

    private const float m_RendererSpacing = 10f;

    private bool m_Newline;
    private bool m_IsCentered;

    private void Start()
    {
        m_Parser = new MarkupParser(m_NormalFont, m_HeadingFont, m_BoldFont, m_ItalicFont, m_LabelPrefab);
        SetChapter(0);

        if (!PoolManager.Pools.ContainsKey("PageRenderer"))
        {
            PoolManager.Pools.Create("PageRenderer");
        }

        for (int i = 0; i < m_Pages.Count; i++)
        {
            var frontRendererGO = PoolManager.Pools["PageRenderer"].Spawn(m_PageRendererPrefab.transform);
            frontRendererGO.SetX(i * 2 * m_RendererSpacing);
            var frontRenderer = frontRendererGO.GetComponent<PageRenderer>();
			frontRenderer.Init();
            frontRenderer.SetIndexInfo(i, true);
            m_FrontPageRenderers.Add(frontRenderer);
            var backRendererGO = PoolManager.Pools["PageRenderer"].Spawn(m_PageRendererPrefab.transform);
            backRendererGO.SetX((i * 2 + 1) * m_RendererSpacing);
            var backRenderer = backRendererGO.GetComponent<PageRenderer>();
			backRenderer.Init();
            backRenderer.SetIndexInfo(i, false);
            m_BackPageRenderers.Add(backRenderer);

            var pageController = m_Pages[i];
            pageController.SetFrontTexture(frontRenderer.OutputTex);
            pageController.SetBackTexture(backRenderer.OutputTex);
            var pageComp = new PageComponents(pageController, frontRenderer, backRenderer);
            m_RightPages.Insert(0, pageComp);
        }

        OrderPos();

        PopulatePage(m_FrontPageRenderers[0], 0, 0, false);
        PopulatePage(m_BackPageRenderers[0], 0, m_FrontPageRenderers[0].LastLocation + 1, false);
        PopulatePage(m_FrontPageRenderers[1], 0, m_BackPageRenderers[0].LastLocation + 1, false);
        PopulatePage(m_BackPageRenderers[1], 0, m_FrontPageRenderers[1].LastLocation + 1, false);
        PopulatePage(m_FrontPageRenderers[2], 0, m_BackPageRenderers[1].LastLocation + 1, false);
        PopulatePage(m_BackPageRenderers[2], 0, m_FrontPageRenderers[2].LastLocation + 1, false);

    }

    private void SetChapter(int chapter)
    {
        if (chapter < 0 || chapter >= m_Chapters.Length)
            TTDebug.LogError("BookController.cs : Chapter is out of range : " + chapter);
                
        m_CurrentChapter = chapter;
        m_Parser.SetParsingText(m_Chapters[m_CurrentChapter].text, m_CurrentChapter);
       /* if (!m_WordPosLookup.ContainsKey(m_Chapters[chapter].name))
        {
            m_WordPosLookup.Add(m_Chapters[chapter].name, m_Parser.GetLookupIndices());
        }*/
    }


    private void PopulatePage(PageRenderer pr, int chapter, int startingIndex, bool reverse)
    {
        bool done = false;
        int currentWord = startingIndex;
        while (!done)
        {
			if(currentWord < 0)
			{
				pr.CompletePage();
				return;
			}
            var nextPageItem = m_Parser.GetElement(currentWord, out m_Newline, out m_IsCentered);
            if (nextPageItem == null)
            {
                // Done with chapter.
				pr.CompletePage();
                return;
            }
            if (!reverse)
            {
                currentWord++;
            }
            else
            {
                currentWord--;
            }

            if (pr.AddItem(nextPageItem, m_Newline, m_IsCentered))
            {
				TTDebug.Log("Added item : " + nextPageItem.name + " : NewLine : " + m_Newline + " : IsCentered : " + m_IsCentered);
                nextPageItem = null;
                continue;
            }
            else
            {
                m_Parser.ReturnElement(nextPageItem);
                done = true;
            }
            
        }
    }

    #region Page Animation

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            PageBack();
        }
        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            PageForward();
        }
    }

    private bool IsPageTurning()
    {
        foreach (var page in m_Pages)
        {
            if (page.IsAnimating)
                return true;
        }
        return false;
    }

    private void PageForward()
    {
        // Do nothing if already animating
        if (IsPageTurning())
            return;

        // Do nothing if there are no more pages to get
        if (m_RightPages.Count == 0)
            return;

        // Turn the page on the right.
        m_TurningPage = m_RightPages[m_RightPages.Count - 1];
        m_TurningPage.Controller.Animate(true);
        m_TurningPage.Controller.AnimateFinishedEvent += FinishForwardTurn;
        if (m_RightPages.Count == 1)
        {
            var lastIndex = m_TurningPage.BackPage.LastLocation;
            if (lastIndex >= m_Parser.ElementCount - 1)
            {
                // Nothing more to read (do chapter transition from here next)
                return;
            }
            var p = m_LeftPages[0];
            m_RightPages.Insert(0, p);
            m_LeftPages.RemoveAt(0);
            p.Controller.m_PageFlip.turn = 0f;
            
            //TTDebug.Log("Setting front page starting at index : " + lastIndex);
            p.FrontPage.ReturnAllItems(PoolManager.Pools["Words"], false);
            PopulatePage(p.FrontPage, 0, lastIndex + 1, false);
            lastIndex = p.FrontPage.LastLocation;
            //TTDebug.Log("Setting back page starting at index : " + lastIndex);
            p.BackPage.ReturnAllItems(PoolManager.Pools["Words"], false);
            PopulatePage(p.BackPage, 0, lastIndex + 1, false);
        }
    }

    private void PageBack()
    {
        if (IsPageTurning())
            return;

        if (m_LeftPages.Count == 0)
        {
            // No more pages there!
            return;
        }

        m_TurningPage = m_LeftPages[m_LeftPages.Count - 1];
        m_TurningPage.Controller.Animate(false);
        m_TurningPage.Controller.AnimateFinishedEvent += FinishBackTrun;
        if (m_LeftPages.Count == 1)
        {
            var firstIndex = m_TurningPage.FrontPage.FirstLocation;
            if (firstIndex <= 0)
            {
                // Nothing more to read (do chapter transition from here next)
                return;
            }
            var p = m_RightPages[0];
            m_LeftPages.Insert(0, p);
            m_RightPages.RemoveAt(0);
            p.Controller.m_PageFlip.turn = 100f;

            p.BackPage.ReturnAllItems(PoolManager.Pools["Words"], true);
            PopulatePage(p.BackPage, 0, firstIndex - 1, true);
            firstIndex = p.BackPage.FirstLocation;
			//TTDebug.Log("Back page goes from : " + p.BackPage.FirstLocation + " to : " + p.BackPage.LastLocation);
            p.FrontPage.ReturnAllItems(PoolManager.Pools["Words"], true);
            PopulatePage(p.FrontPage, 0, firstIndex - 1, true);
			//TTDebug.Log("Front page goes from : " + p.FrontPage.FirstLocation + " to : " + p.FrontPage.LastLocation);
        }
    }

    private void FinishForwardTurn()
    {
        m_RightPages.Remove(m_TurningPage);
        m_LeftPages.Add(m_TurningPage);
        m_TurningPage.Controller.AnimateFinishedEvent -= FinishForwardTurn;
        m_TurningPage = null;
        OrderPos();
    }

    private void FinishBackTrun()
    {
        m_LeftPages.Remove(m_TurningPage);
        m_RightPages.Add(m_TurningPage);
        m_TurningPage.Controller.AnimateFinishedEvent -= FinishBackTrun;
        m_TurningPage = null;
        OrderPos();
    }

    private void OrderPos()
    {
        for (int i = m_LeftPages.Count - 1; i >= 0; i--)
        {
            m_LeftPages[i].Controller.transform.position = Vector3.up * i * 0.001f;
        }

        for (int i = m_RightPages.Count - 1; i >= 0; i--)
        {
            m_RightPages[i].Controller.transform.position = Vector3.up * i * 0.001f;
        }
    }

    #endregion
}
