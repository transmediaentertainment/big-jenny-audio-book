using UnityEngine;
using System.Collections.Generic;
using System.Globalization;
using TransTech.System.Debug;
using System.Text;
using UnityExtensions;
using TransTech.Books.Pages;

namespace TransTech.System.Markup
{
    public class MarkupParser
    {
        private UIFont m_NormalFont;
        private UIFont m_HeadingFont;
        private UIFont m_BoldFont;
        private UIFont m_ItalicFont;

        private UIFont m_CurrentFont;
        private UIFont m_NextFont;
        private Color m_CurrentColor;
        private Color m_NextColor;
        private readonly Color m_DefaultColor = new Color(1f, 1f, 1f);
        private string m_CurrentNarrationFileName;
        private float m_CurrentNarrationFileLength;
        private float m_CurrentNarrationWordLength;
        private float m_CurrentNarrationWordStart;

        private List<string> m_CurrentMusicFiles = new List<string>();
        private List<bool> m_CurrentMusicIsLooping = new List<bool>();
        private List<string> m_CurrentMusicInstanceIDs = new List<string>();
        private List<float> m_CurrentMusicStartTimes = new List<float>();

        private List<string> m_CurrentEffectFiles = new List<string>();

        private List<string> m_CurrentAtmosphereFiles = new List<string>();
        private List<string> m_CurrentAtmosphereInstanceIDs = new List<string>();
        private List<bool> m_CurrentAtmosphereIsLooping = new List<bool>();
        private List<float> m_CurrentAtmosphereStartTimes = new List<float>();

        private string m_Text;
        private int m_TextLocation;

        private GameObject m_LabelPrefab;

        public int ElementCount { get { return m_PageElements.Count; } }

        
        public MarkupParser(UIFont normalFont, UIFont headingFont, UIFont boldFont, UIFont italicFont, GameObject labelPrefab)
        {
            m_NormalFont = normalFont;
            m_HeadingFont = headingFont;
            m_BoldFont = boldFont;
            m_ItalicFont = italicFont;
            m_LabelPrefab = labelPrefab;
        }

        public void SetParsingText(string text, int chapter)
        {
            m_Text = text;
            m_CurrentChapter = chapter;
            m_TextLocation = 0;
            m_CurrentNarrationWordLength = 0f;
            m_CurrentNarrationWordStart = 0f;
            m_CurrentColor = m_DefaultColor;
            m_NextColor = m_DefaultColor;
            ParseText();
        }

        private int m_CurrentChapter;

        public List<PageElement> m_PageElements = new List<PageElement>();

        StringBuilder sb = new StringBuilder();
        bool shouldStartNewElement = false;
        bool shouldJustify = false;
        bool newlineInWord = false;
        int elementLocation = 0;
        float narrationTime = 0f;
        string narrationFile = null;

        private void ResetParsingVars()
        {
            sb.Clear();
            shouldStartNewElement = false;
            newlineInWord = false;
            elementLocation++;
            if (m_NextFont != null)
            {
                m_CurrentFont = m_NextFont;
                m_NextFont = null;
            }
            shouldJustify = m_CurrentFont != m_HeadingFont;
            // Color is a struct, so we can't set it to null
            m_CurrentColor = m_NextColor;
            m_CurrentNarrationWordStart += m_CurrentNarrationWordLength;
        }

        private void ParseText()
        {
            while (m_TextLocation < m_Text.Length)
            {
                var c = CurrentChar();

                switch (c)
                {
                    case '|': // We have hit a tag
                        if (shouldStartNewElement)
                        {
                            AddWord(sb.ToString(), elementLocation, m_CurrentFont, m_CurrentColor, newlineInWord, shouldJustify, narrationFile, narrationTime);
                            ResetParsingVars();
                        }
                        ReadTag();
                        break;
                    case ' ': // End of a word
                        shouldStartNewElement = true;
                        sb.Append(c);
						m_TextLocation++;
                        break;
                    case '\n':// New line,
                        newlineInWord = true;
                        shouldStartNewElement = true;
                        m_TextLocation++;
                        break;
                    case '\r':// New line
                        newlineInWord = true;
                        shouldStartNewElement = true;
                        m_TextLocation++;
                        break;
                    default: //  We are reading a word
                        if (shouldStartNewElement)
                        {
                            AddWord(sb.ToString(), elementLocation, m_CurrentFont, m_CurrentColor, newlineInWord, shouldJustify, narrationFile, narrationTime);
                            ResetParsingVars();
                        }   
                        sb.Append(c);
                        m_TextLocation++;
                        break;
                }
            }
			
			if(sb.Length > 0)
			{
				AddWord(sb.ToString(), elementLocation, m_CurrentFont, m_CurrentColor, newlineInWord, shouldJustify, narrationFile, narrationTime);
                ResetParsingVars();
			}
        }

        private void AddWord(string word, int elementLocation, UIFont font, Color wordColor, bool newLine, bool justify, string narrationFile, float narrationTime)
        {
            var element = new WordElement(font, wordColor, word, newLine, justify, narrationFile, m_CurrentNarrationWordStart, narrationTime, m_CurrentChapter, elementLocation);
            m_PageElements.Add(element);
			//TTDebug.Log("Adding Word : " + word + " : Count : " + m_PageElements.Count);
        }

        private char CurrentChar()
        {
            if (m_TextLocation >= m_Text.Length)
                return ' ';
            return m_Text[m_TextLocation];
        }

        private void ReadTag()
        {
            m_TextLocation++;
            // Now at first letter
            var firstChar = CurrentChar();
            switch (firstChar)
            {
                case '/':
                    m_TextLocation++;
                    ReadClosingTag();
                    break;
                case 'A':
                    m_TextLocation++;
                    ReadAudioTag();
                    break;
                case 'I':
                    m_TextLocation++;
                    var secondChar = CurrentChar();
                    if (secondChar == 'M')
                    {
                        AddImageElement();
                        ResetParsingVars();
                    }
                    else if (secondChar == '|')
                    {
                        m_CurrentFont = m_ItalicFont;
                        m_TextLocation++;
                    }
                    break;
                case 'C':
                    ReadNewColour();
                    break;
                case 'B':
                    m_CurrentFont = m_BoldFont;
                    m_TextLocation += 2;
                    break;
                case 'H':
                    m_CurrentFont = m_HeadingFont;
                    shouldJustify = false;
                    m_TextLocation += 2;
                    break;
                default:
                    TTDebug.LogError("MarkupParser.cs : Unsupported Tag Encountered : " + firstChar);
                    break;
            }
        }

        private void ReadNewColour()
        {
            m_TextLocation += 2;
            string red = m_Text[m_TextLocation].ToString() + m_Text[m_TextLocation + 1].ToString();
            m_TextLocation += 2;
            string green = m_Text[m_TextLocation].ToString() + m_Text[m_TextLocation + 1].ToString();
            m_TextLocation += 2;
            string blue = m_Text[m_TextLocation].ToString() + m_Text[m_TextLocation + 1].ToString();
            int r = int.Parse(red, NumberStyles.HexNumber);
            int g = int.Parse(green, NumberStyles.HexNumber);
            int b = int.Parse(blue, NumberStyles.HexNumber);
            m_CurrentColor = new Color(r / 255f, g / 255f, b / 255f);
            m_TextLocation += 3; // To get to the first character after the tag
        }

        private void AddImageElement()
        {
            int diff;
            m_TextLocation += 3;
            var pos = GetSubString(m_TextLocation, ':', out diff);
            m_TextLocation += diff + 1;
            var percentText = GetSubString(m_TextLocation, ':', out diff);
            m_TextLocation += diff + 1;
            var fileName = GetSubString(m_TextLocation, '|', out diff);
            m_TextLocation += diff + 1;
            // Make sure we use up the m_Newline characters
            while (m_Text[m_TextLocation] == '\n' || m_Text[m_TextLocation] == '\r')
            {
                m_TextLocation++;
            }
            ImageAlignment al;
            if (pos == "Left")
                al = ImageAlignment.Left;
            else if (pos == "Centre")
                al = ImageAlignment.Center;
            else if (pos == "Right")
                al = ImageAlignment.Right;
            else
            {
                TTDebug.LogError("MarkupParser.cs : Couldn't determine image alignmnet : " + pos + " defaulting to centre");
                al = ImageAlignment.Center;
            }
            var size = float.Parse(percentText);
            var element = new ImageElement(al, fileName, size, m_CurrentChapter, elementLocation);
            m_PageElements.Add(element);
            //newlineInWord = true;
        }

        private void ReadAudioTag()
        {
            var c = CurrentChar();
            string temp;
            int diff;
            string type;
            switch (c)
            {
                case 'N':
                    m_TextLocation += 2;
                    temp = GetSubString(m_TextLocation, ':', out diff);
                    m_CurrentNarrationFileLength = float.Parse(temp);
                    m_TextLocation += diff + 1;
                    m_CurrentNarrationFileName = GetSubString(m_TextLocation, '|', out diff);
                    m_TextLocation += diff + 1;
                    m_CurrentNarrationWordStart = 0f;
                    break;
                case 'L':
                    m_TextLocation += 2;
                    temp = GetSubString(m_TextLocation, '|', out diff);
                    m_CurrentNarrationWordLength = float.Parse(temp);
                    m_TextLocation += diff + 1;
                    break;
                case 'M':
                    m_TextLocation += 2;
                    type = GetSubString(m_TextLocation, ':', out diff);
                    m_CurrentMusicIsLooping.Add(type == "Loop");
                    m_TextLocation += diff + 1;
                    m_CurrentMusicInstanceIDs.Add(GetSubString(m_TextLocation, ':', out diff));
                    m_TextLocation += diff + 1;
                    m_CurrentMusicFiles.Add(GetSubString(m_TextLocation, '|', out diff));
                    m_TextLocation += diff + 1;
                    m_CurrentMusicStartTimes.Add(0f);
                    break;
                case 'E':
                    m_TextLocation += 2;
                    m_CurrentEffectFiles.Add(GetSubString(m_TextLocation, '|', out diff));
                    m_TextLocation += diff + 1;
                    break;
                case 'A':
                    m_TextLocation += 2;
                    type = GetSubString(m_TextLocation, ':', out diff);
                    m_CurrentAtmosphereIsLooping.Add(type == "Loop");
                    m_TextLocation += diff + 1;
                    m_CurrentAtmosphereInstanceIDs.Add(GetSubString(m_TextLocation, ':', out diff));
                    m_TextLocation += diff + 1;
                    m_CurrentAtmosphereFiles.Add(GetSubString(m_TextLocation, '|', out diff));
                    m_TextLocation += diff + 1;
                    m_CurrentAtmosphereStartTimes.Add(0f);
                    break;
                default:
                    TTDebug.LogError("MarkupParser.cs : Unsupported Audio Open Tag encountered : " + c);
                    break;

            }
        }

        private void ReadClosingTag()
        {
            var c = CurrentChar();
            switch (c)
            {
                case 'I':
                    m_NextFont = m_NormalFont;
                    m_TextLocation += 2;
                    break;
                case 'B':
                    m_NextFont = m_NormalFont;
                    m_TextLocation += 2;
                    break;
                case 'C':
                    m_NextColor = m_DefaultColor;
                    m_TextLocation += 2;
                    break;
                case 'H':
                    m_NextFont = m_NormalFont;
                    m_TextLocation += 2;
                    break;
                case 'A':
                    m_TextLocation++;
                    ReadAudioClosingTag();
                    break;
                default:
                    TTDebug.LogError("MarkupParser.cs : Unsupported Closing tag encountered : " + c);
                    break;
            }
        }

        private void ReadAudioClosingTag()
        {
            var c = CurrentChar();
            string instance;
            int diff;
            int location;
            switch (c)
            {
                case 'N': // Audio Narration
                    m_CurrentNarrationFileName = "";
                    m_CurrentNarrationFileLength = 0f;
                    m_TextLocation += 2;
                    break;
                case 'M': // Audio Music
                    m_TextLocation += 2;
                    instance = GetSubString(m_TextLocation, '|', out diff);
                    location = m_CurrentMusicInstanceIDs.IndexOf(instance);
                    if (location == -1)
                        TTDebug.LogError("Couldn't find Music Instance ID : " + instance);
                    m_CurrentMusicInstanceIDs.RemoveAt(location);
                    m_CurrentMusicIsLooping.RemoveAt(location);
                    m_CurrentMusicFiles.RemoveAt(location);
                    m_TextLocation += diff + 1;
                    break;
                case 'A': // Audio Atmosphere
                    m_TextLocation += 4;
                    instance = GetSubString(m_TextLocation, '|', out diff);
                    location = m_CurrentAtmosphereInstanceIDs.IndexOf(instance);
                    if (location == -1)
                        TTDebug.LogError("Couldn't find Atmosphere Instance ID : " + instance);
                    m_CurrentAtmosphereInstanceIDs.RemoveAt(location);
                    m_CurrentAtmosphereIsLooping.RemoveAt(location);
                    m_CurrentAtmosphereFiles.RemoveAt(location);
                    break;
                default:
                    TTDebug.Log("MarkupParser.cs : Unsupported Audio closing tag encountered : " + c);
                    break;
            }
        }

        private int GetDiffToNextChar(int start, char c)
        {
            int diff = 0;
            while (m_Text[start + diff] != c)
                diff++;
            return diff;
        }

        private string GetSubString(int start, char endChar, out int diff)
        {
            diff = 0;
            while (m_Text[start + diff] != endChar)
                diff++;
            return m_Text.Substring(start, diff);
        }
		
		public GameObject GetElement(int index, out bool newLine, out bool isCentered)
		{
            if (index >= m_PageElements.Count)
            {
                TTDebug.Log("End of elements!");
                newLine = false;
                isCentered = false;
                return null;
            }
			//TTDebug.Log("Index : " + index + " Elements : " + m_PageElements.Count);
			var element = m_PageElements[index];
			if(element is WordElement)
			{
				var we = element as WordElement;

                var t = PoolManager.Pools["Words"].Spawn(m_LabelPrefab.transform);
                var go = t.gameObject;
				go.name = we.Word;

                var label = go.GetComponent<UILabel>();
                label.text = we.Word;
                label.font = we.Font;
                label.color = we.WordColour;

                var data = t.GetComponent<WordData>();

                data.SetAudioNarration(we.NarrationFile, we.NarrationWordLength, we.NarrationStartTime);
                data.SetElementLocation(element.ChapterLocation, element.ElementLocation);
                newLine = we.NewLine;
                isCentered = !we.Justify;
				//TTDebug.Log("Getting element for word : " +we.Word + " : at index : " + index);
				
                return t.gameObject;
			}
            else if (element is ImageElement)
            {
                var ie = element as ImageElement;
                var tex = ImageDictionary.Instance.GetImageFromDictionary(ie.Image);
                // Default planes are 10x10 units
                var plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
                plane.layer = LayerMask.NameToLayer("GUI");
                plane.renderer.material.mainTexture = tex;
                var d = plane.AddComponent<ImageData>();
                d.SetData(ie.Size);
                d.SetElementLocation(element.ChapterLocation, element.ElementLocation);
                newLine = true;
                isCentered = true;
				//TTDebug.Log("Getting image at index : " + index);
                return plane;
            }
            else
            {
                TTDebug.LogError("Unsupported Element type encountered!");
                newLine = true;
                isCentered = false;
                return null;
            }
		}

        public void ReturnElement(GameObject go)
        {
            PoolManager.Pools["Words"].Despawn(go.transform);
        }
    
        public GameObject ReadWord(out bool includesNewLine, out bool isCentered)
        {
            int diff;
            string newWord = "";
            isCentered = m_CurrentFont == m_HeadingFont;

            while (m_TextLocation < m_Text.Length && m_Text[m_TextLocation] != ' ' && m_Text[m_TextLocation] != '\n' && m_Text[m_TextLocation] != '\r')
            {
                // If we hit a markup spot
                if (m_Text[m_TextLocation] == '|')
                {
                    // Begin markup
                    m_TextLocation++;
                    switch (m_Text[m_TextLocation])
                    {
                        case 'B': // Bold
                            m_CurrentFont = m_BoldFont;
                            m_TextLocation += 2; // |
                            break;
                        case 'I': // Italic/Image
                            if (m_Text[m_TextLocation + 1] == '|') // Italic
                            {
                                m_CurrentFont = m_ItalicFont;
                                m_TextLocation += 2;
                            }
                            else if (m_Text[m_TextLocation + 1] == 'M') // Image
                            {
                                m_TextLocation += 4;
                                var pos = GetSubString(m_TextLocation, ':', out diff);
                                m_TextLocation += diff + 1;
                                var percentText = GetSubString(m_TextLocation, ':', out diff);
                                m_TextLocation += diff + 1;
                                var fileName = GetSubString(m_TextLocation, '|', out diff);
                                m_TextLocation += diff + 1;
                                // Make sure we use up the m_Newline characters
                                while (m_Text[m_TextLocation] == '\n' || m_Text[m_TextLocation] == '\r')
                                {
                                    m_TextLocation++;
                                }
                                var tex = ImageDictionary.Instance.GetImageFromDictionary(fileName);
                                // Default planes are 10x10 units
                                var plane = GameObject.CreatePrimitive(PrimitiveType.Plane);
                                plane.layer = LayerMask.NameToLayer("GUI");
                                var percent = float.Parse(percentText);
                                plane.renderer.material.mainTexture = tex;
                                var d = plane.AddComponent<ImageData>();
                                d.SetData(percent);
                                includesNewLine = true;
                                return plane;
                            }
                            else
                            {
                                TTDebug.LogError("Unknown I based markup");
                            }
                            break;
                        case 'H': // Heading
                            m_CurrentFont = m_HeadingFont;
                            isCentered = true;
                            m_TextLocation += 2;
                            break;
                        case 'C': // Colour
                            m_TextLocation += 2;
                            string red = m_Text[m_TextLocation].ToString() + m_Text[m_TextLocation + 1].ToString();
                            m_TextLocation += 2;
                            string green = m_Text[m_TextLocation].ToString() + m_Text[m_TextLocation + 1].ToString();
                            m_TextLocation += 2;
                            string blue = m_Text[m_TextLocation].ToString() + m_Text[m_TextLocation + 1].ToString();
                            int r = int.Parse(red, NumberStyles.HexNumber);
                            int g = int.Parse(green, NumberStyles.HexNumber);
                            int b = int.Parse(blue, NumberStyles.HexNumber);
                            m_CurrentColor = new Color(r / 255f, g / 255f, b / 255f);
                            break;
                        case 'A': // Audio Narration / Audio Timestamp / Audio Music / Audio Effect / Audio Atmosphere
                            string temp = "";
                            string type = "";
                            switch (m_Text[m_TextLocation + 1])
                            {
                                case 'N': // Audio Narration
                                    m_TextLocation += 3;
                                    temp = GetSubString(m_TextLocation, ':', out diff);
                                    m_CurrentNarrationFileLength = float.Parse(temp);
                                    m_TextLocation += diff + 1;
                                    m_CurrentNarrationFileName = GetSubString(m_TextLocation, '|', out diff);
                                    m_TextLocation += diff + 1;
                                    m_CurrentNarrationWordStart = 0f;
                                    break;
                                case 'L': // Audio Length
                                    m_TextLocation += 3;
                                    temp = GetSubString(m_TextLocation, '|', out diff);
                                    m_CurrentNarrationWordLength = float.Parse(temp);
                                    m_TextLocation += diff + 1;
                                    break;
                                case 'M': // Audio Music
                                    m_TextLocation += 3;
                                    type = GetSubString(m_TextLocation, ':', out diff);
                                    m_CurrentMusicIsLooping.Add(type == "Loop");
                                    m_TextLocation += diff + 1;
                                    m_CurrentMusicInstanceIDs.Add(GetSubString(m_TextLocation, ':', out diff));
                                    m_TextLocation += diff + 1;
                                    m_CurrentMusicFiles.Add(GetSubString(m_TextLocation, '|', out diff));
                                    m_TextLocation += diff + 1;
                                    m_CurrentMusicStartTimes.Add(0f);
                                    break;
                                case 'E': // Audio Effects
                                    m_TextLocation += 3;
                                    m_CurrentEffectFiles.Add(GetSubString(m_TextLocation, '|', out diff));
                                    m_TextLocation += diff + 1;
                                    break;
                                case 'A': // Audio Atmosphere
                                    m_TextLocation += 3;
                                    type = GetSubString(m_TextLocation, ':', out diff);
                                    m_CurrentAtmosphereIsLooping.Add(type == "Loop");
                                    m_TextLocation += diff + 1;
                                    m_CurrentAtmosphereInstanceIDs.Add(GetSubString(m_TextLocation, ':', out diff));
                                    m_TextLocation += diff + 1;
                                    m_CurrentAtmosphereFiles.Add(GetSubString(m_TextLocation, '|', out diff));
                                    m_TextLocation += diff + 1;
                                    m_CurrentAtmosphereStartTimes.Add(0f);
                                    break;
                                default:
                                    TTDebug.LogError("Unknown A based markup : " + m_Text[m_TextLocation + 1]);
                                    break;
                            }
                            break;
                        case '/': // Closing tags
                            switch (m_Text[m_TextLocation + 1])
                            {
                                case 'C': // Colour
                                    m_CurrentColor = m_DefaultColor;
                                    m_TextLocation += 3;
                                    break;
                                case 'B': // Bold
                                    m_NextFont = m_NormalFont;
                                    m_TextLocation += 3;
                                    break;
                                case 'H': // Heading
                                    m_NextFont = m_NormalFont;
                                    m_TextLocation += 3;
                                    break;
                                case 'A':
                                    string instance = "";
                                    int location;
                                    switch (m_Text[m_TextLocation + 2])
                                    {
                                        case 'N': // Audio Narration
                                            m_CurrentNarrationFileName = "";
                                            m_CurrentNarrationFileLength = 0f;
                                            m_TextLocation += 4;
                                            break;
                                        case 'M': // Audio Music
                                            m_TextLocation += 4;
                                            instance = GetSubString(m_TextLocation, '|', out diff);
                                            location = m_CurrentMusicInstanceIDs.IndexOf(instance);
                                            if (location == -1)
                                                TTDebug.LogError("Couldn't find Music Instance ID : " + instance);
                                            m_CurrentMusicInstanceIDs.RemoveAt(location);
                                            m_CurrentMusicIsLooping.RemoveAt(location);
                                            m_CurrentMusicFiles.RemoveAt(location);
                                            break;
                                        case 'A': // Audio Atmosphere
                                            m_TextLocation += 4;
                                            instance = GetSubString(m_TextLocation, '|', out diff);
                                            location = m_CurrentAtmosphereInstanceIDs.IndexOf(instance);
                                            if (location == -1)
                                                TTDebug.LogError("Couldn't find Atmosphere Instance ID : " + instance);
                                            m_CurrentAtmosphereInstanceIDs.RemoveAt(location);
                                            m_CurrentAtmosphereIsLooping.RemoveAt(location);
                                            m_CurrentAtmosphereFiles.RemoveAt(location);
                                            break;
                                        default:
                                            TTDebug.LogError("Unknown A Based escape tag : " + m_Text[m_TextLocation + 2].ToString());
                                            break;
                                    }
                                    break;
                            }
                            break;
                        default:
                            TTDebug.LogError("Invalid Character at " + m_TextLocation + " : " + m_Text[m_TextLocation] + " : ASCII : " + ((int)m_Text[m_TextLocation]).ToString());
                            break;
                    }
                }
                else
                {
                    newWord += m_Text[m_TextLocation].ToString();
                    m_TextLocation++;
                }
            }
			
			// Increment past our space character
            includesNewLine = false;
            // Catch for if we've hit the end already or not
			if(m_TextLocation < m_Text.Length)
			{
	            while (m_Text[m_TextLocation] == ' ' || m_Text[m_TextLocation] == '\n' || m_Text[m_TextLocation] == '\r')
	            {
                    // Just flag the boolean so the new line character doesn't end up in the string.
	                if (m_Text[m_TextLocation] == '\n' || m_Text[m_TextLocation] == '\r')
	                    includesNewLine = true;
                    else
                        newWord += m_Text[m_TextLocation].ToString(); // Only add the space (No newlines)
	                m_TextLocation++;
                    // If we hit the end of the text
	                if (m_TextLocation >= m_Text.Length)
	                    break;
	            }
			}

            var t = PoolManager.Pools["Words"].Spawn(m_LabelPrefab.transform);
            var go = t.gameObject;
			
			var label = go.GetComponent<UILabel>();
			label.text = newWord;
			label.font = m_CurrentFont;

            if (m_NextFont != null)
            {
                m_CurrentFont = m_NextFont;
                m_NextFont = null;
            }
			
            var data = go.GetComponent<WordData>();
            if (m_CurrentNarrationFileName != "")
            {
                data.SetAudioNarration(m_CurrentNarrationFileName, m_CurrentNarrationWordLength, m_CurrentNarrationWordStart);
            }

            if (m_CurrentMusicFiles.Count > 0)
            {
                for (int i = 0; i < m_CurrentMusicFiles.Count; i++)
                {
                    if (m_CurrentMusicIsLooping[i])
                        data.AddMusicData(m_CurrentMusicFiles[i], -1f);
                    else
                    {
                        data.AddMusicData(m_CurrentMusicFiles[i], m_CurrentMusicStartTimes[i]);
                        m_CurrentMusicStartTimes[i] += m_CurrentNarrationWordLength;
                    }
                }
            }

            if (m_CurrentAtmosphereFiles.Count > 0)
            {
                for (int i = 0; i < m_CurrentAtmosphereFiles.Count; i++)
                {
                    if(m_CurrentAtmosphereIsLooping[i])
                        data.AddAtmosphereData(m_CurrentAtmosphereFiles[i], -1f);
                    else
                    {
                        data.AddAtmosphereData(m_CurrentAtmosphereFiles[i], m_CurrentAtmosphereStartTimes[i]);
                        m_CurrentAtmosphereStartTimes[i] += m_CurrentNarrationWordLength;
                    }
                }
            }

            if(m_CurrentEffectFiles.Count > 0)
            {
                for (int i = 0; i < m_CurrentEffectFiles.Count; i++)
                {
                    data.AddEffectData(m_CurrentEffectFiles[i]);
                }
            }

            // We clear all effects once posted
            m_CurrentEffectFiles.Clear();

            // We increment the word start point after setting the current data
            m_CurrentNarrationWordStart += m_CurrentNarrationWordLength;
            m_CurrentNarrationWordLength = 0f;

            return go;
        }
    }
}
