using UnityEngine;
using System.Collections;

public class ImageData : ElementData {

    public float PagePercentage { get; private set; }

    public void SetData(float percent)
    {
        PagePercentage = percent;
    }
}
