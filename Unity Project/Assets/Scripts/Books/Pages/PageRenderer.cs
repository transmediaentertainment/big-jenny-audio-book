using System.Collections.Generic;
using TransTech.System.Debug;
using UnityEngine;
using UnityExtensions;

/// <summary>
/// Class for rendering text, images, etc. to a render texture
/// </summary>
public class PageRenderer : MonoBehaviour 
{
    public UIPanel m_Panel;
    public UICamera m_Cam;
    public UILabel m_SetupLabel;

    private List<bool> m_LineIsCentered = new List<bool>();
    private List<float> m_LineHeights = new List<float>();
    private List<List<GameObject>> m_PageItems = new List<List<GameObject>>();
    private List<GameObject> m_CurrentLine;

    private float m_MinLineHeight;
    private float m_LineSpaceTotal;
    private float m_LineSpaceLeft;
    private float m_LineTopPos;
    private float m_LineMidPos;
    private float m_LineBottomPos;

    private Vector3 m_TopRight; // 1,1
    private Vector3 m_BottomLeft; // 0,0

    private bool m_IsReverse;

    public RenderTexture OutputTex { get; private set; }

    public float m_BorderSpacing = 0.1f;

    public int FirstLocation
    {
        get
        {
            if (m_PageItems == null || m_PageItems.Count == 0)
                return -1;
            if (m_PageItems[0] == null || m_PageItems[0].Count == 0)
                return -1;
            return GetLocation(m_PageItems[0][0]);
        }
    }

    public int LastLocation
    {
        get
        {
            if (m_PageItems == null || m_PageItems.Count == 0)
                return -1;
            int c1 = m_PageItems.Count - 1;
            if (m_PageItems[c1] == null || m_PageItems[c1].Count == 0)
                return -1;
            int c2 = m_PageItems[c1].Count - 1;
            return GetLocation(m_PageItems[c1][c2]);
        }
    }

    private int GetLocation(GameObject go)
    {
        var element = go.GetComponent<ElementData>();
        if (element == null)
            return -1;
        return element.ElementLocation;
    }

    public int RendererIndex { get; private set; }
    public bool IsFrontRenderer { get; private set; }

    public void SetIndexInfo(int index, bool isFront)
    {
        RendererIndex = index;
        IsFrontRenderer = isFront;
    }

    public void Init()
    {
        var cam = m_Cam.camera;
        cam.enabled = false;

        // Initialize the render texture.  Fixed size for now.
        OutputTex = new RenderTexture(511, 720, 24, RenderTextureFormat.Default);
        OutputTex.wrapMode = TextureWrapMode.Repeat;
        OutputTex.Create();

        cam.targetTexture = OutputTex;
        cam.rect = new Rect(0f, 0f, 1f, 1f);

        m_BottomLeft = cam.ViewportToWorldPoint(new Vector3(0f, 0f, cam.nearClipPlane));
        m_TopRight = cam.ViewportToWorldPoint(new Vector3(1f, 1f, cam.nearClipPlane));
        // Add in our spacing
        m_BottomLeft.x += m_BorderSpacing;
        m_BottomLeft.y += m_BorderSpacing;
        m_TopRight.x -= m_BorderSpacing;
        m_TopRight.y -= m_BorderSpacing;

        m_LineSpaceTotal = m_TopRight.x - m_BottomLeft.x;

        m_LineTopPos = m_TopRight.y;
        m_LineBottomPos = m_TopRight.y;
        m_LineMidPos = m_TopRight.y;
        m_IsReverse = false;

        m_MinLineHeight = Vector3.Scale(m_SetupLabel.relativeSize, m_SetupLabel.transform.lossyScale).y;
        m_SetupLabel.gameObject.SetActive(false);
        NewLine();
    }

    /// <summary>
    /// Adds a new item to the page.
    /// </summary>
    /// <param name="item">The item to be added to the page</param>
    /// <param name="newLineAtEnd">Should we start a new line after adding this item?</param>
    /// <param name="isCentered">Should the item's line be centered? (or Justified)</param>
    /// <returns>False if unable to add the item to the page</returns>
    public bool AddItem(GameObject item, bool newLineAtEnd, bool isCentered)
    {
        if (m_IsReverse && newLineAtEnd)
            NewLine();
        var label = item.GetComponent<UILabel>();
        var img = item.GetComponent<ImageData>();
        if (isCentered && !m_LineIsCentered[m_LineIsCentered.Count - 1])
            m_LineIsCentered[m_LineIsCentered.Count - 1] = isCentered;
        if (label != null) // If it's a label
        {
            item.transform.parent = m_Panel.transform;
            item.transform.localScale = new Vector3(label.font.size, label.font.size, 1f);
            var worldSize = Vector3.Scale(label.relativeSize, label.transform.lossyScale);
            if (worldSize.x > m_LineSpaceLeft)
			{
                if (!NewLine()) // If we can't fit the word on the page
                {
                    item.transform.parent = null;
                    VerticalJustifyPage();
                    CompletePage();
                    return false;
                }
				else
				{
					// We have triggered a new line and need to bring the centering information through
					if (isCentered && !m_LineIsCentered[m_LineIsCentered.Count - 1])
            			m_LineIsCentered[m_LineIsCentered.Count - 1] = isCentered;
				}
			}
            
            // Add Label
            var height = m_LineTopPos - m_LineBottomPos;
            if (worldSize.y > height)
            {
                // Check we have enough space left
                if ((!m_IsReverse && m_LineTopPos - worldSize.y < m_BottomLeft.y) ||
                    (m_IsReverse && m_LineBottomPos + worldSize.y > m_TopRight.y))
                {
                    // We don't have enough room left!
                    item.transform.parent = null;
                    VerticalJustifyPage();
					// We have already added another line, so remove it
                    m_PageItems.RemoveLast();
                    CompletePage();
                    return false;
                }
                
                // Resize the line vertically
                if (m_IsReverse)
                {
                    m_LineTopPos = m_LineBottomPos + worldSize.y;
                }
                else
                {
                    m_LineBottomPos = m_LineTopPos - worldSize.y;
                }
                m_LineMidPos = m_LineBottomPos + (worldSize.y * 0.5f);
                for (int i = 0; i < m_CurrentLine.Count; i++)
                {
                    var pos = m_CurrentLine[i].transform.position;
                    pos.y = m_LineMidPos;
                    m_CurrentLine[i].transform.position = pos;
                }
            }
            
            // Set the horizontal position
            if (m_IsReverse)
            {
                m_CurrentLine.Insert(0, item);
                // We need to reset the positions in reverse mode
                var pos = 0f;
                for(int i = 0; i < m_CurrentLine.Count; i++)
                {
					var l = m_CurrentLine[i].GetComponent<UILabel>();
					var ws = Vector3.Scale(l.relativeSize, l.transform.lossyScale);
                    l.transform.position = new Vector3(m_BottomLeft.x + pos + (ws.x * 0.5f), m_LineMidPos, 0f);
                    pos += ws.x;
                }
            }
            else
            {
                m_CurrentLine.Add(item);
                label.transform.position = new Vector3(m_BottomLeft.x + m_LineSpaceTotal - m_LineSpaceLeft + (worldSize.x * 0.5f),
                    m_LineMidPos, 0f);
                
            }
            m_LineSpaceLeft -= worldSize.x;
        }
        else if (img != null) // If it's an Image
        {
            item.transform.rotation = Quaternion.Euler(270, 0f, 0f);

            float percent = img.PagePercentage;
            float pageWidth = m_LineSpaceTotal * percent;
            var tex = img.renderer.material.mainTexture;
            var ratio = (float)tex.height / tex.width;
            // Due to the rotation of the plane, we scale z for vertical rather than y
            item.transform.localScale = new Vector3(pageWidth / 10f, pageWidth / 10f, (pageWidth * ratio) / 10f);
            item.transform.parent = m_Panel.transform;

            var b = item.renderer.bounds;
            var height = m_LineTopPos - m_LineBottomPos;
            if (b.size.y > height)
            {
                if (m_IsReverse)
                {
                    m_LineTopPos = m_LineBottomPos + b.size.y;
                }
                else
                {
                    m_LineBottomPos = m_LineTopPos - b.size.y;
                }
                m_LineMidPos = m_LineBottomPos + (b.size.y * 0.5f);
                for (int i = 0; i < m_CurrentLine.Count; i++)
                {
                    var pos = m_CurrentLine[i].transform.position;
                    pos.y = m_LineMidPos;
                    m_CurrentLine[i].transform.position = pos;
                }
            }
            item.transform.position = new Vector3(0f, m_LineMidPos, 0f);
            m_CurrentLine.Add(item);
        }
        else
        {
            TTDebug.LogError("I really shouldn't be in here!");
        }
        
        if (newLineAtEnd && !m_IsReverse)
            NewLine();
        return true;
    }
	
	public void CompletePage()
	{
		TTDebug.Log("Page Completed from : " + FirstLocation + " To : " + LastLocation);
		m_RenderCount = 0;
        SystemEventCenter.Instance.LateUpdateEvent += Render;	
	}

    public void ReturnAllItems(SpawnPool returnPool, bool setReverse)
    {
        for (int i = 0; i < m_PageItems.Count; i++)
        {
            for (int j = 0; j < m_PageItems[i].Count; j++)
            {
                if (m_PageItems[i][j].GetComponent<ImageData>() != null)
                {
                    // we aren't pooling images right now
                    Destroy(m_PageItems[i][j]);
                    m_PageItems[i][j] = null;
                }
                else
                {
                    returnPool.Despawn(m_PageItems[i][j].transform);
                    m_PageItems[i][j] = null;
                }
            }
        }

        // This will probably need a better pooling system.  Dropping a lot for the GC to collect
        m_PageItems.Clear();

        // Reset the top of the page 
        if (setReverse)
        {
            m_LineBottomPos = m_BottomLeft.y;
            m_LineMidPos = m_BottomLeft.y;
            m_LineTopPos = m_BottomLeft.y;
        }
        else
        {
            m_LineTopPos = m_TopRight.y;
            m_LineBottomPos = m_TopRight.y;
            m_LineMidPos = m_TopRight.y;
        }
        m_IsReverse = setReverse;

        m_LineIsCentered.Clear();
        m_LineHeights.Clear();
        m_CurrentLine = null;

        NewLine();
    }

    private int m_RenderCount = 0;

    /// <summary>
    /// Waits a frame before rendering the camera.  This is due to 
    /// needing to wait for NGUI's batching system.
    /// </summary>
    /// <param name="deltaTime">The time since the last frame</param>
    private void Render(float deltaTime)
    {
        if (m_RenderCount >= 2)
        {
            var rt = RenderTexture.active;
            RenderTexture.active = m_Cam.camera.targetTexture;
            m_Cam.camera.Render();
            RenderTexture.active = rt;
            SystemEventCenter.Instance.LateUpdateEvent -= Render;
        }
        m_RenderCount++;
    }

    /// <summary>
    /// Sets the page to a new line
    /// </summary>
    /// <returns>False if we have reached the end of the page</returns>
    private bool NewLine()
    {
        // TODO : Justify the previous line
        if (m_CurrentLine != null)
        {
			if(m_IsReverse)
				m_LineHeights.Insert(0, m_LineTopPos - m_LineBottomPos);
			else
            	m_LineHeights.Add(m_LineTopPos - m_LineBottomPos);

            if ((m_IsReverse && m_LineIsCentered[0]) || 
				(!m_IsReverse && m_LineIsCentered[m_LineIsCentered.Count - 1]))
            {
                var totalXSize = 0f;
                for (int i = 0; i < m_CurrentLine.Count; i++)
                {
                    totalXSize += GetWorldSize(m_CurrentLine[i]).x;
                }
                
                float pos = -totalXSize * 0.5f + transform.position.x;
                for (int i = 0; i < m_CurrentLine.Count; i++)
                {
                    var worldSize = GetWorldSize(m_CurrentLine[i]);
                    m_CurrentLine[i].transform.SetX(pos + (worldSize.x * 0.5f));
                    pos += worldSize.x;
                }
            }
            else
            {
                var extraSpacing = m_LineSpaceLeft / (m_CurrentLine.Count - 1);
                var addOnSpace = 0f;
                for (int i = 0; i < m_CurrentLine.Count; i++)
                {
                    m_CurrentLine[i].transform.SetX(m_CurrentLine[i].transform.position.x + addOnSpace);
                    addOnSpace += extraSpacing;
                }
            }
        }

        m_LineSpaceLeft = m_LineSpaceTotal;
		if(m_IsReverse)
		{
			m_LineBottomPos = m_LineTopPos;
			m_LineMidPos = m_LineTopPos;
		}
		else
		{
	        m_LineTopPos = m_LineBottomPos;
	        m_LineMidPos = m_LineBottomPos;
		}
        if (!m_IsReverse && m_LineBottomPos < m_BottomLeft.y + m_BorderSpacing)
            return false;
		else if(m_IsReverse && m_LineTopPos > m_TopRight.y - m_BorderSpacing)
			return false;

        m_CurrentLine = new List<GameObject>();
        m_LineIsCentered.Add(false);
		if(m_IsReverse)
			m_PageItems.Insert(0, m_CurrentLine);
		else
			m_PageItems.Add(m_CurrentLine);

        return true;
    }

    private Vector3 GetWorldSize(GameObject go)
    {
        var label = go.GetComponent<UILabel>();
        if (label != null)
            return Vector3.Scale(label.relativeSize, label.transform.lossyScale);
        var imgData = go.GetComponent<ImageData>();
        if (imgData != null)
            return imgData.transform.lossyScale * 10f;
        
        TTDebug.LogError("PageRenderer.cs : Unable to determine size of item in line!");
        return Vector3.zero;
    }

    private void VerticalJustifyPage()
    {
        float totalHeight = m_TopRight.y - m_BottomLeft.y;
        float usedHeight = 0f;
        foreach (var f in m_LineHeights)
        {
            usedHeight += f;
        }
        if (usedHeight > totalHeight)
        {
            TTDebug.LogError("Used height is over total height.  Y U NO MAKE SENSE! " + totalHeight + " : " + usedHeight);
        }
        float remainingHeight = totalHeight - usedHeight;
        float inc = remainingHeight / (m_PageItems.Count - 1);
        float addOn = inc;
        for (int i = 1; i < m_PageItems.Count; i++)
        {
            for (int j = 0; j < m_PageItems[i].Count; j++)
            {
				if(m_IsReverse)
					m_PageItems[i][j].transform.SetY(m_PageItems[i][j].transform.position.y + addOn);
				else
                	m_PageItems[i][j].transform.SetY(m_PageItems[i][j].transform.position.y - addOn);
            }
            addOn += inc;
        }
    }
}
