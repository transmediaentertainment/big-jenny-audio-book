using UnityEngine;
using System.Collections.Generic;

public class WordData : ElementData
{
    public string AudioNarrationFile { get; private set; }
    public float AudioNarrationLength { get; private set; }
    public float AudioNarrationStartTime { get; private set; } // From start of narration file

    public void SetAudioNarration(string file, float length, float startTime)
    {
        AudioNarrationFile = file;
        AudioNarrationLength = length;
        AudioNarrationStartTime = startTime;
    }

    private List<string> m_MusicFiles = new List<string>();
    private List<float> m_MusicStartTimes = new List<float>();

    public void AddMusicData(string file, float startTime)
    {
        m_MusicFiles.Add(file);
        m_MusicStartTimes.Add(startTime);
    }

    private List<string> m_AtmosphereFiles = new List<string>();
    private List<float> m_AtmosphereStartTimes = new List<float>();

    public void AddAtmosphereData(string file, float startTime)
    {
        m_AtmosphereFiles.Add(file);
        m_AtmosphereStartTimes.Add(startTime);
    }

    private List<string> m_Effects = new List<string>();

    public void AddEffectData(string file)
    {
        m_Effects.Add(file);
    }


}
