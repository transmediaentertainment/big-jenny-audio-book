using UnityEngine;

namespace TransTech.Books.Pages
{
    public enum ImageAlignment
    {
        Left, 
        Right,
        Center
    }

    public class ImageElement : PageElement
    {
        public ImageAlignment Alignment { get; private set; }
        public string Image { get; private set; }
        public float Size { get; private set; }

        public ImageElement(ImageAlignment alignment, string image, float size, int chapterLocation, int elementLocation)
            : base(chapterLocation, elementLocation)
        {
            Alignment = alignment;
            Image = image;
            Size = size;
        }
    }
}