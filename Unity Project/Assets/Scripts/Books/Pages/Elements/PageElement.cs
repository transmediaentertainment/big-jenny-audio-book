namespace TransTech.Books.Pages
{
    public abstract class PageElement
    {
        public int ElementLocation { get; protected set; }
        public int ChapterLocation { get; protected set; }

        protected PageElement(int chapterLocation, int elementLocation)
        {
            ElementLocation = elementLocation;
            ChapterLocation = chapterLocation;
        }
    }
}