using UnityEngine;

namespace TransTech.Books.Pages
{
    public class WordElement : PageElement
    {
        public UIFont Font { get; private set; }
        public Color WordColour { get; private set; }
        public string Word { get; private set; }
        public bool NewLine { get; private set; }
        public bool Justify { get; private set; }
        public string NarrationFile { get; private set; }
        public float NarrationWordLength { get; private set; }
        public float NarrationStartTime { get; private set; }

        public WordElement(UIFont font, Color wordColor, string word, bool newLine, bool justify, string narrationFile,
            float narrationStartTime, float narrationTime, int chapterLocation, int elementLocation)
            : base(chapterLocation, elementLocation)
        {
            Font = font;
            WordColour = wordColor;
            Word = word;
            NewLine = newLine;
            Justify = justify;
            NarrationFile = narrationFile;
            NarrationStartTime = narrationStartTime;
            NarrationStartTime = narrationTime;
        }
    }
}