using UnityEngine;
using System.Collections;

public abstract class ElementData : MonoBehaviour 
{
    public int ElementLocation { get; protected set; }
    public int ChapterLocation { get; protected set; }

    public void SetElementLocation(int chapter, int element)
    {
        ElementLocation = element;
        ChapterLocation = chapter;
    }
}
