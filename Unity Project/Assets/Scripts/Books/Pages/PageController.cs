using UnityEngine;
using System.Collections;
using TransTech.Delegates;

public class PageController : MonoBehaviour 
{
    public MegaPageFlip m_PageFlip;
    public MeshRenderer m_Renderer;

    private Material m_FrontMaterial;
    private Material m_BackMaterial;

    private bool m_AnimateForward;
    public bool IsAnimating { get; private set; }
    private float m_Timer;
    private const float m_AnimateTime = 0.75f;

    public event VoidDelegate AnimateFinishedEvent;

    private void Awake()
    {
        var shader = Shader.Find("Diffuse");
        m_Renderer.materials[0] = new Material(shader);
        m_Renderer.materials[1] = new Material(shader);
        m_FrontMaterial = m_Renderer.materials[0];
        m_BackMaterial = m_Renderer.materials[1];
    }

    public void SetFrontTexture(RenderTexture tex)
    {
        m_FrontMaterial.mainTexture = tex;
    }

    public void SetBackTexture(RenderTexture tex)
    {
        m_BackMaterial.mainTexture = tex;
    }

    public void Animate(bool forward)
    {
        if (!IsAnimating)
        {
            m_AnimateForward = forward;
            m_Timer = 0f;
            SystemEventCenter.Instance.UpdateEvent += AnimateUpdate;
            IsAnimating = true;
        }
    }

    private void AnimateUpdate(float deltaTime)
    {
        m_Timer += deltaTime;
        if (m_Timer >= m_AnimateTime)
        {
            SystemEventCenter.Instance.UpdateEvent -= AnimateUpdate;
            IsAnimating = false;
            m_Timer = m_AnimateTime;
            if (AnimateFinishedEvent != null)
                AnimateFinishedEvent();
        }
        if (m_AnimateForward)
            m_PageFlip.turn = Mathf.Lerp(0f, 100f, m_Timer / m_AnimateTime);
        else
            m_PageFlip.turn = Mathf.Lerp(100f, 0f, m_Timer / m_AnimateTime);
    }
}
