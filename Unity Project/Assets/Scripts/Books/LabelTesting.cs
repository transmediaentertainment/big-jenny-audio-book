using UnityEngine;
using System.Collections;

public class LabelTesting : MonoBehaviour 
{
    public UILabel m_Hello;
    public UILabel m_World;
    public UILabel m_Users;

    private void Awake()
    {
        Debug.Log("Bleh");
        Debug.Log("Hello : " + m_Hello.relativeSize.x);
        Debug.Log("World : " + m_World.relativeSize.x);
        Debug.Log("Users : " + m_Users.relativeSize.x);

        Debug.Log("Hello Calc : " + m_Hello.font.CalculatePrintedSize("Hello ", false, UIFont.SymbolStyle.None));
        Debug.Log("Scaled : " + Vector3.Scale(m_Hello.font.CalculatePrintedSize("Hello ", false, UIFont.SymbolStyle.None), m_Hello.transform.lossyScale).x);
    }
}
