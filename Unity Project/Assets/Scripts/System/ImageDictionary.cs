using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// This class handles the dictionary of images for the book to use.
/// </summary>
public class ImageDictionary : MonoSingleton<ImageDictionary>
{
	/// <summary>
	/// The dictionary of images.
	/// </summary>
	private Dictionary<string, Texture2D> m_ImageDictionary;
	/// <summary>
	/// The image to use when an image is missing.
	/// </summary>
	[SerializeField]
	private Texture2D m_ImageMissing;
	/// <summary>
	/// The list of images to be put in the dictionary for easy lookup. Assign in Unity Inspector.
	/// </summary>
	[SerializeField]
	private List<Texture2D> m_ImageList;
	
	/// <summary>
	/// Called when the class initialises.
	/// </summary>
	void Awake()
	{
		m_ImageDictionary = new Dictionary<string, Texture2D>();
		
		if( m_ImageDictionary == null )
		{
			Debug.LogError("ImageDictionary.cs : ImageDictionary is null.");
		}
		
		if( m_ImageMissing == null )
		{
			Debug.LogError("ImageDictionary.cs : GenericImage is null. You must assign it in the Unity inspector.");
		}
		
		if( m_ImageList == null || m_ImageList.Count <= 0 )
		{
			Debug.LogError("ImageDictionary.cs : ImageList is empty or null. You must populate it in the Unity inspector.");
		}
		
		// Put each image from the list into the dictionary for easy lookup.
		foreach( var image in m_ImageList )
		{
			m_ImageDictionary.Add( image.name, image );
		}
	}
	
	/// <summary>
	/// Gets an image from the image dictionary.
	/// </summary>
	/// <returns>The image (Texture2D) from the image dictionary.</returns>
	/// <param name='imageName'>The name of the image to get.</param>
	public Texture2D GetImageFromDictionary( string imageName )
	{
		if( m_ImageDictionary.ContainsKey(imageName) )
		{
			return m_ImageDictionary[imageName];
		}
		else
		{
			Debug.LogWarning("ImageDictionary.cs : Image name was not found in Image Dictionary. Returning generic image instead. : " + imageName);
			return m_ImageMissing;
		}
	}
}
