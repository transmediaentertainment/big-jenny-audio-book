using System.Runtime.InteropServices;

/// <summary>
/// The bridge to iOS native code
/// </summary>
public static class iOSBridge
{
	[DllImport("__Internal")]
	extern static public void SetBaseURL( string baseURL );
	
	[DllImport("__Internal")]
	extern static public void LoadChapterPlaylistFile( string fileName, int audioPlayerTypeIndex );
	
	[DllImport("__Internal")]
	extern static public void PlayAudio( int chapterIndex, float time );
	
	[DllImport("__Internal")]
	extern static public string GetCurrentPosition();
}