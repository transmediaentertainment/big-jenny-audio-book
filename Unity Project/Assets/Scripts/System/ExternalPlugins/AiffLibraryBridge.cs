using System.Runtime.InteropServices;

public static class AiffLibraryBridge {
	
	[DllImport ("AiffLibrary")]
	public static extern string PrintHello();

	[DllImport ("AiffLibrary")]
	public static extern int OpenFile( string fileName );

	[DllImport ("AiffLibrary")]
	public static extern int CloseFile();

	[DllImport ("AiffLibrary")]
	public static extern float AIFF_ReadNextMarker();	
}
