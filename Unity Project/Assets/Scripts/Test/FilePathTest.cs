using UnityEngine;
using System.Collections;

enum AudioPlayers
{
    Narration,
    Music,
    Effects,
    Atmosphere
};

public struct PositionInfo
{
	public int Chapter;
	public int Index;
	public float Time;
}

public class FilePathTest : MonoBehaviour
{	
	void Start ()
	{
		iOSBridge.SetBaseURL( Application.dataPath + "/Raw/Audio/" );
		
		iOSBridge.LoadChapterPlaylistFile( "Narration/C1.txt", (int)AudioPlayers.Narration );
		iOSBridge.LoadChapterPlaylistFile( "Music/C1.txt", (int)AudioPlayers.Music );
		iOSBridge.LoadChapterPlaylistFile( "Effects/C1.txt", (int)AudioPlayers.Effects );
		
		iOSBridge.LoadChapterPlaylistFile( "Narration/C1.txt", (int)AudioPlayers.Narration );
		iOSBridge.LoadChapterPlaylistFile( "Music/C1.txt", (int)AudioPlayers.Music );
		iOSBridge.LoadChapterPlaylistFile( "Effects/C1.txt", (int)AudioPlayers.Effects );
	
		iOSBridge.PlayAudio( 0, 0.0f );
	}
	
	float time;
	bool done = false;
	
	// Update is called once per frame
	void Update ()
	{
		time += Time.deltaTime;
		
		if( time > 4.0f )
		{
			SplitPositionInfo( iOSBridge.GetCurrentPosition() );
			done = !done;
			time = 0.0f;
		}
	}
	
	void SplitPositionInfo( string positionInfo )
	{
		string[] splits = positionInfo.Split(new char[] { ',' }, System.StringSplitOptions.None);
		
		PositionInfo info = new PositionInfo();
		
		info.Chapter = System.Convert.ToInt32( splits[0] );
		info.Index = System.Convert.ToInt32( splits[1] );
		info.Time = System.Convert.ToSingle( splits[2] );
		
		Debug.Log( "Ch: " + info.Chapter + " Indx: " + info.Index + " Time: " + info.Time );
	}
}
