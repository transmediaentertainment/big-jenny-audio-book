using UnityEngine;
using System.Collections;

public class AiffTest : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
		Debug.Log(AiffLibraryBridge.PrintHello());
		string filename = Application.dataPath + "/StreamingAssets/Audio/Narration/BJKT-C1-S1.aif";
		
		int retval = AiffLibraryBridge.OpenFile(filename);
		Debug.Log( "Open R: " + retval + " N: " + filename );
		
		float seconds;
		
		do {
			seconds = AiffLibraryBridge.AIFF_ReadNextMarker();
			Debug.Log("Marker S: " + seconds );
		} while ( seconds != -1.0 );
		
		AiffLibraryBridge.CloseFile();
		Debug.Log( "Close R: " + retval );
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
