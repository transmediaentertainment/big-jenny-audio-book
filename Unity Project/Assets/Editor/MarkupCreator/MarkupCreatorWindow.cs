using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using TransTech.Delegates;
using System.IO;
using System.Text.RegularExpressions;

/// <summary>
/// The AudioFileData class is used to store information about the audio files to be played with the text
/// </summary>
public class AudioFileData {
	
	/// <summary>
	/// The word count of the word within the sentence that defines the time to start playing this audio file (file starts playing at the start of this word)
	/// </summary>
	public int m_StartWord;
	/// <summary>
	/// the filename of the audio file to play
	/// </summary>
	public string m_Filename;
	/// <summary>
	/// The length of the audio file as a float
	/// a length of 0 will indicate that the data has not been read from the aiff file or manually entered. 
	/// </summary>
	public float m_Length;
	
	public AudioFileData( int startWord, string filename ) {
		m_StartWord = startWord;
		m_Filename = filename;
		m_Length = 0;
	}
}

/// <summary>
/// the WordData class us used to store all of the data associated with a word
/// </summary>
public class MarkupWordData {
	/// <summary>
	/// The word
	/// </summary>
	public string m_Word;
	/// <summary>
	/// The length of the word in seconds as a float (as determined by the marker in the aif file - or specified manually)
	/// </summary>
	public float m_WordLength;
	/// <summary>
	/// The length of the sentence to the end of this word as a float (calculated by adding the length of the current word to the previous words total length)
	/// </summary>
	public float m_TotalLength;
	/// <summary>
	/// Any text modifiers applied to this word (italic, bold etc.)
	/// </summary>
	public string m_TextModifier;
	
	public MarkupWordData( string word, float wordLength, float previousTotalLength, string textModifier )
	{
		m_Word = word;
		m_WordLength = wordLength;
		m_TotalLength = previousTotalLength + m_WordLength;
		m_TextModifier = textModifier;
	}
}	

/// <summary>
/// The MarkupSentenceData class is used to store all of the data for each sentence in the chapter.
/// </summary>
public class MarkupSentenceData {
	
	/// <summary>
	/// The number of the section within this chapter where the sentence is located
	/// </summary>
	public int m_SectionNumber;
	/// <summary>
	/// The number of the sentence within this chapter
	/// </summary>
	public int m_SentenceNumber;
	/// <summary>
	/// The text of the sentence (including the . at the end, excluding \n)
	/// </summary>
	public string m_SentenceText;
	/// <summary>
	/// The text of the sentence broken down into words along with the duration and modifier data
	/// </summary>
	public List<MarkupWordData> m_SentenceWords;
	/// <summary>
	/// The narration file associated with this sentence - Highlander (there can be only one)
	/// </summary>
	public AudioFileData m_NarrationFile;
	/// <summary>
	/// The list of music files starting a some point during this sentence
	/// </summary>
	public List<AudioFileData> m_MusicFiles;
	/// <summary>
	/// The list of atmos files starting a some point during this sentence
	/// </summary>
	public List<AudioFileData> m_AtmosFiles;
	/// <summary>
	/// The list of effects files starting a some point during this sentence
	/// </summary>
	public List<AudioFileData> m_EffectsFiles;
	/// <summary>
	/// The length of this sentence in seconds as a float - this should be the same as the length of the narration audio file - may not be needed - we may just use the length of the narration audio file...
	/// a length of 0 will indicate that the data has not been read from the aiff file or manually entered.
	/// </summary>
	public float m_Length;
	
	public MarkupSentenceData( int sectionNumber, int sentenceNumber, string sentenceText, string narrationFilename ) {
		m_SectionNumber = sectionNumber;
		m_SentenceNumber = sentenceNumber;
		m_SentenceText = sentenceText;
		m_SentenceWords = new List<MarkupWordData>();
		float totalLength = 0.0f;
		char[] splitChars = { ' ' };
		foreach( var word in m_SentenceText.Split( splitChars, System.StringSplitOptions.RemoveEmptyEntries) )
		{
			MarkupWordData newWord = new MarkupWordData(word, 0.5f, totalLength, "" );
			m_SentenceWords.Add( newWord );
			totalLength = newWord.m_TotalLength;
		}
		m_NarrationFile = new AudioFileData( 0, narrationFilename );
		m_MusicFiles = null;
		m_AtmosFiles = null;
		m_EffectsFiles = null;
		m_Length = 0;
	}
	
}

public class MarkupCreatorWindow : EditorWindow {

	static bool m_initialised = false;
	static string m_NarrationFilePrefix = "BJKT";
	static MarkupCreatorWindow m_Window;
	static string[] noChapterFiles = {"No Chapter Files"};
	static string[] chapterFiles = null;
	static List<MarkupSentenceData> m_sentenceList = null;
	
	static GUIStyle m_SentenceStyle;
	static GUIStyle m_SectionSentenceLabelStyle;

	private static void Init()
	{
		if (m_initialised) return;
		m_initialised = true;

		m_Window = EditorWindow.GetWindow<MarkupCreatorWindow>();

		m_sentenceList = new List<MarkupSentenceData> ();
		MyAllPostprocessor.TextFileChangedEvent += m_Window.ReloadChapterFiles;
		m_Chapter = -1;
		m_Window.ReloadChapterFiles ();
		m_SentenceStyle = new GUIStyle (EditorGUIUtility.GetBuiltinSkin (EditorSkin.Inspector).GetStyle ("Button"));
		m_SentenceStyle.alignment = TextAnchor.MiddleLeft;
		m_SectionSentenceLabelStyle = new GUIStyle (EditorGUIUtility.GetBuiltinSkin (EditorSkin.Inspector).GetStyle ("Button"));
		m_SectionSentenceLabelStyle.alignment = TextAnchor.MiddleCenter;
	}
	
	[MenuItem ("Window/Markup Creator")]
	public static void OpenWindow()
	{
		if ( m_Window == null ) 
		{
			Init();
		}
		
		m_Window.Show();
	}

  	Vector2 listScrollPos;
  	Vector2 selectedSentenceScrollPos;

	static int m_Chapter = 0;
	static int m_SelectedSentence = 0;
    int index = 0;
	
	void OnGUI()
	{
		if ( !m_initialised ) Init();

		GUILayout.Label ("Select Chapter", EditorStyles.boldLabel);
		
		index = EditorGUILayout.Popup(index,  (chapterFiles != null ) ? chapterFiles : noChapterFiles );
		if ( index != m_Chapter && chapterFiles != null )
		{
			m_Chapter = index;
			// Change chapter
			LoadChapterText(chapterFiles[m_Chapter]);
		}
		EditorGUILayout.Space();

		EditorGUILayout.BeginHorizontal();
		GUILayout.Button ("Sec/Sen", GUILayout.Width(60) );
		GUILayout.Button ("Book Text Lines", GUILayout.Width(150) );
		GUILayout.Button ("Length", GUILayout.Width(50));
		GUILayout.Button ("Narration Files", GUILayout.Width(150) );
		GUILayout.Button ("Music Files", GUILayout.Width(150) );
		GUILayout.Button ("Atmos Files", GUILayout.Width(150) );
		GUILayout.Button ("Effects Files", GUILayout.Width(150) );
        EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginHorizontal();
		
        listScrollPos = EditorGUILayout.BeginScrollView(listScrollPos, GUILayout.Height (150));
		
		EditorGUILayout.BeginVertical();
		foreach ( var sentenceData in m_sentenceList )
		{
			EditorGUILayout.BeginHorizontal();
			
			Color c = GUI.color;
			
			if ( m_SelectedSentence == sentenceData.m_SentenceNumber )
			{
				GUI.color = Color.green;
			}
			
	        if ( GUILayout.Button( sentenceData.m_SectionNumber + "/" + sentenceData.m_SentenceNumber, m_SectionSentenceLabelStyle, GUILayout.Width(60) ) ) 
			{
				m_SelectedSentence = sentenceData.m_SentenceNumber;
			}
	        if ( GUILayout.Button( sentenceData.m_SentenceText, m_SentenceStyle, GUILayout.Width(150) ) )
			{
				m_SelectedSentence = sentenceData.m_SentenceNumber;
			}
	        if ( GUILayout.Button( sentenceData.m_Length.ToString(), m_SentenceStyle, GUILayout.Width(50) ) )
			{
				m_SelectedSentence = sentenceData.m_SentenceNumber;
			}
	        if ( GUILayout.Button( sentenceData.m_NarrationFile.m_Filename, m_SentenceStyle, GUILayout.Width(150) ) )
			{
				m_SelectedSentence = sentenceData.m_SentenceNumber;
			}
			
			GUI.color = c;
	        EditorGUILayout.EndHorizontal();		
		}
        EditorGUILayout.EndVertical();
		
        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndHorizontal();
		
		EditorGUILayout.BeginVertical();
        GUILayout.Button( m_sentenceList[m_SelectedSentence].m_SectionNumber + "/" + m_sentenceList[m_SelectedSentence].m_SentenceNumber, m_SectionSentenceLabelStyle, GUILayout.Width(60) );
		
        selectedSentenceScrollPos = EditorGUILayout.BeginScrollView( selectedSentenceScrollPos );

		EditorGUILayout.BeginHorizontal( "box" );
		foreach( var word in m_sentenceList[m_SelectedSentence].m_SentenceWords )
		{
			EditorGUILayout.BeginVertical();
        	GUILayout.Button( word.m_Word, m_SectionSentenceLabelStyle, GUILayout.Width(120) );
			EditorGUILayout.BeginHorizontal( GUILayout.Width(120) );
        	GUILayout.Button( word.m_WordLength.ToString(), m_SectionSentenceLabelStyle, GUILayout.Width(50) );
        	GUILayout.Button( word.m_TotalLength.ToString(), m_SectionSentenceLabelStyle, GUILayout.Width(50) );
	        EditorGUILayout.EndHorizontal();
	        EditorGUILayout.EndVertical();
		}
        EditorGUILayout.EndHorizontal();
		
        EditorGUILayout.EndScrollView();
        EditorGUILayout.EndVertical();
		
	}
	
	/// <summary>
	/// Splits the section string up into sentences, delimited by .!?
	/// </summary>
	/// <returns>
	/// An array of sentence strings
	/// </returns>
	/// <param name='sectionString'>
	/// Section string to parse and split
	/// </param>
	string[] SplitSection( string sectionString )
	{
		List<string> returnList = new List<string>();
		
		int startIndex = 0;
		int charCounter;
		bool addNewString = false;
		
		for (charCounter=0; charCounter<sectionString.Length; charCounter++)
		{
			switch( sectionString[charCounter] )
			{
			case '.':
			case '!':
			case '?':
				if ( charCounter >= sectionString.Length-1 )
				{
					// we are at the end of the string
					addNewString = true;
					break;
				}
				else
				{
					charCounter++;
					// we can check the next character
					switch ( sectionString[charCounter] )
					{
					case ' ':
						// delimit
						addNewString = true;
						break;
					case '"':
					case (char)8221:	// this is the right double quotation mark - see http://www.ascii.cl/htmlcodes.htm for a full list of chars
						if ( charCounter >= sectionString.Length-1 ) 
						{
							// we are at the end of the string
							addNewString = true;
							break;
						}
						else 
						{
							charCounter++;
							switch( sectionString[charCounter] )
							{
							case ' ':
								addNewString = true;
								break;
							}
						}
						break;
					}
				}
				break;
			}
			if ( addNewString || charCounter >= (sectionString.Length-1) )
			{
				string stringToBeAdded = sectionString.Substring(startIndex,charCounter+1-startIndex);
				//Debug.Log("AddNewString:" + stringToBeAdded );
				returnList.Add(stringToBeAdded);
				startIndex = charCounter+1;
			}
			addNewString = false;
			if ( charCounter >= sectionString.Length )
			{
				//Debug.Log("End of Split Section");
				break;
			}
		}
		return returnList.ToArray();
	}
	
	void LoadChapterText( string chapterFile )
	{
		string[] chapterFileText = chapterFile.Split('.');
		string chapterPrefix = chapterFileText[0];
		Debug.Log("Load: " + chapterFile + "[:" + chapterPrefix + "]"  );
		
		StreamReader chapterFilereader = new System.IO.StreamReader( Application.dataPath + "/Book Content Folder/Text/" + chapterFile );
		string chapterText = chapterFilereader.ReadToEnd();
		chapterFilereader.Close();
		
		string sectionText;
		string[] sentenceTextArray;
		
		int sectionCounter = 0;
		int sentenceCounter = 0;
		
		using (System.IO.StringReader chapterReader = new System.IO.StringReader(chapterText)) {
			
    		while ((sectionText = chapterReader.ReadLine()) != null)
			{
				sentenceTextArray = SplitSection( sectionText );
				foreach ( var sentence in sentenceTextArray )
				{
					Debug.Log( "Sec:" + sectionCounter + " Sen:" + sentenceCounter + " >" + sentence );
					var sentenceData = new MarkupSentenceData( sectionCounter, sentenceCounter, sentence, m_NarrationFilePrefix + "-" + chapterPrefix + "-S" + sentenceCounter );
					m_sentenceList.Add(sentenceData);
					sentenceCounter++;
				}
				sectionCounter++;
			}
		}		
		
	}
	
	void ReloadChapterFiles()
	{
		bool refresh = false;
		Debug.Log("Reload");
		
		//string d
			
		if ( !Directory.Exists( Application.dataPath + "/Book Content Folder" ) )
		{
			Directory.CreateDirectory( Application.dataPath + "/Book Content Folder" );
			refresh = true;
		}
		
		if ( !Directory.Exists( Application.dataPath + "/Book Content Folder/Text" ) )
		{
			Directory.CreateDirectory( Application.dataPath + "/Book Content Folder/Text" );
			refresh = true;
		}
		
		if ( refresh )
		{
			AssetDatabase.Refresh();
		}
		
		chapterFiles = Directory.GetFiles( Application.dataPath + "/Book Content Folder/Text", "*.txt" );
		
		for ( int i=0; i<chapterFiles.Length; i++ )
		{
			Debug.Log("CF: " + chapterFiles[i] );
			var splits = chapterFiles[i].Split( '/' );
			chapterFiles[i] = splits[ splits.Length - 1 ];
		}
		
	}

}


public class MyAllPostprocessor : AssetPostprocessor {
	
	public static event VoidDelegate TextFileChangedEvent;
	
	static void OnPostprocessAllAssets ( string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths )
	{
	    foreach (string str in importedAssets)
		{
			if ( str.Contains("Book Content Folder") )
			{
				if ( TextFileChangedEvent != null ) {
					TextFileChangedEvent();
				}
			}
	    	Debug.Log("Reimported Asset: " + str);
		}
		foreach (string str in deletedAssets)
		{
	    	Debug.Log("Deleted Asset: " + str);
		}
	    for (var i=0;i<movedAssets.Length;i++)
		{
	    	Debug.Log("Moved Asset: " + movedAssets[i] + " from: " + movedFromAssetPaths[i]);
	    }
	}
}