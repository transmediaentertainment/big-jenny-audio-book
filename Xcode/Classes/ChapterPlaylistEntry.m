//
//  ChapterPlaylistEntry.m
//  Unity-iPhone
//
//  Created by Simon on 24/04/13.
//
//

#import "ChapterPlaylistEntry.h"

@interface ChapterPlaylistEntry ()

@end

@implementation ChapterPlaylistEntry

- (id)initWithTime:(float)time andFilename: (NSString*)filename
{
    self = [super init];
    _time = time;
    _filename = [[NSString alloc] initWithString:filename];
    return self;
}

-(void)dealloc
{
    [super dealloc];
    [_filename release];
    _filename = nil;
}

@end