//
//  AudioChannel.h
//  Unity-iPhone
//
//  Created by Simon on 24/04/13.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "ChapterPlaylistEntry.h"
#import "AudioSubSystem.h"

typedef enum
{
    AP_Narration,
    AP_Music,
    AP_Effects,
    AP_Atmosphere,
    AP_COUNT
} AudioPlayerTypes;

@interface AudioChannel : NSObject <AVAudioPlayerDelegate>

-(id)initWithType:(AudioPlayerTypes)type andDirectoryName:(NSString*)directoryName;
-(ChapterPlaylistEntry*)getNextEntryAndIncrement:(BOOL)increment;
-(void)playAudioWithEntry:(ChapterPlaylistEntry*)entry nextEntry:(ChapterPlaylistEntry*)nextEntry andTime:(float)time;
-(void)stopAudio;

@property (nonatomic) AudioPlayerTypes type;
@property (nonatomic, retain) AVAudioPlayer* audioPlayer;
@property (nonatomic, retain) NSMutableArray* playlistEntries;
@property (nonatomic, retain) NSString* directoryName;
@property (nonatomic) int entryIndex;
@property (nonatomic) float time;

@end
