//
//  AudioChannel.m
//  Unity-iPhone
//
//  Created by Simon on 24/04/13.
//
//

#import "AudioChannel.h"

@interface AudioChannel ()

@end

@implementation AudioChannel

-(id)initWithType:(AudioPlayerTypes)type andDirectoryName:(NSString*)directoryName
{
    self = [super init];
    _type = type;
    _playlistEntries = [[NSMutableArray alloc] init];
    _directoryName = [[NSString alloc] initWithString:directoryName];
    _entryIndex = 0;
    return self;
}

-(void)dealloc
{
    [super dealloc];
    [_audioPlayer release];
    _audioPlayer = nil;
    [_playlistEntries release];
    _playlistEntries = nil;
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag
{
    if( flag )
    {
        ChapterPlaylistEntry *nextEntry = nil;
        
        NSArray* chapterPlaylistEntries = _playlistEntries[[AudioSubSystem audioSubSystemSingleton].currentChapter];
        ChapterPlaylistEntry* currentEntry = chapterPlaylistEntries[_entryIndex];
        _time = ([currentEntry time] + _audioPlayer.duration);
        
        nextEntry = [self getNextEntryAndIncrement:YES];
        
        // If we assigned next playlist entry, play it!
        if( nextEntry != nil )
        {
            [self playAudioWithEntry:nextEntry nextEntry:nil andTime:_time];
        }
    }
    else
    {
        NSLog(@"Error: AudioChannel : audio play did finish playing unsuccessfully.");
    }
}

-(ChapterPlaylistEntry*)getNextEntryAndIncrement:(BOOL)increment
{
    NSArray* chapterPlaylistEntries = _playlistEntries[[AudioSubSystem audioSubSystemSingleton].currentChapter];
    
    // If the next playlist entry is not beyond the current playlist entries: increment to the next entry
    if( ( _entryIndex + 1 ) < chapterPlaylistEntries.count )
    {
        if( increment )
        {
            ++_entryIndex;
        }
        return chapterPlaylistEntries[_entryIndex];
    }
    // Else if the next chapter is not beyond the current book: incriment the chapter and set current playlist entry to 0, and inform the aux channels to start the next chapter
    else if( ( [AudioSubSystem audioSubSystemSingleton].currentChapter + 1 ) < _playlistEntries.count )
    {
        if( _type == AP_Narration )
        {
            _entryIndex = 0;
            _time = 0.0f;
            chapterPlaylistEntries = _playlistEntries[++[AudioSubSystem audioSubSystemSingleton].currentChapter];
            // Call notification to aux audio channels
            [[AudioSubSystem audioSubSystemSingleton] playAuxAudioWithChapterIndex:[AudioSubSystem audioSubSystemSingleton].currentChapter time:0.0f andPlayer:AP_Music];
            [[AudioSubSystem audioSubSystemSingleton] playAuxAudioWithChapterIndex:[AudioSubSystem audioSubSystemSingleton].currentChapter time:0.0f andPlayer:AP_Effects];
            
            return chapterPlaylistEntries[_entryIndex];
        }
        else
        {
            NSLog(@"End of chapter reached for %@.", _directoryName);
        }
        
    }
    // Else: reached end of book
    else
    {
        NSLog(@"End of book reached for %@.", _directoryName);
        
        if( _type == AP_Narration )
        {
            [[AudioSubSystem audioSubSystemSingleton] stopAudio];
        }
    }
    
    return nil;
}

-(void)playAudioWithEntry:(ChapterPlaylistEntry*)entry nextEntry:(ChapterPlaylistEntry*)nextEntry andTime:(float)time
{
    // Release and null the previous audio player
    [_audioPlayer release];
    _audioPlayer = nil;
    
    // Get the URL from the found entry
    NSURL* url = [NSURL fileURLWithPath:[entry filename]];
    
    if( url != nil )
    {
        // Init the audio player with the URL
        _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
        
        if( _audioPlayer != nil )
        {
            // If the time requested is before a file is queued to start: queue the current entry to play at the correct time
            if( time - [entry time] < 0 )
            {
                NSLog(@">>>>Queue. AudioPlayer: %@ Time: %f, Entry Time: %f", _directoryName, time, [entry time]);
                [_audioPlayer setDelegate:self];
                [_audioPlayer playAtTime:_audioPlayer.deviceCurrentTime + (NSTimeInterval)([entry time] - time)];
            }
            
            // Else if the time requested is after the current files' duration: queue the next entry to play at the correct time
            else if( time - [entry time] > _audioPlayer.duration )
            {
                NSLog(@">>>>Next. AudioPlayer: %@ Time: %f, Entry Time: %f", _directoryName, time, [entry time]);
                [_audioPlayer release];
                _audioPlayer = nil;
                
                if( nextEntry == nil )
                {
                    NSLog(@"Error: AudioChannel.mm : Trying to access next entry without having passed one through.");
                    return;
                }
                
                // Get the URL from the next entry
                NSURL* url = [NSURL fileURLWithPath:[nextEntry filename]];
                
                if( url != nil )
                {
                    // Init the audio player with the URL and set its delegate to this class
                    _audioPlayer = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:nil];
                    if( _audioPlayer != nil )
                    {
                        [_audioPlayer setDelegate:self];
                        [_audioPlayer playAtTime:_audioPlayer.deviceCurrentTime + (NSTimeInterval)([nextEntry time] - time)];
                    }
                }
                else
                {
                    NSLog(@"Error: AudioSubSystem.mm : URL was invalid. Audio Player: %@, URL: %@", _directoryName, url);
                }
            }
            // Else: track to the time needed and play the audio file
            else
            {
                NSLog(@">>>>Now. AudioPlayer: %@ Time: %f, Entry Time: %f", _directoryName, time, [entry time]);
                [_audioPlayer setDelegate:self];
                [_audioPlayer setCurrentTime:time - [entry time]];
                [_audioPlayer play];
            }
        }
    }
    else
    {
        NSLog(@"Error: AudioSubSystem.mm : URL was invalid. Audio Player: %@, URL: %@", _directoryName, url);
    }
}

-(void)stopAudio
{
    [_audioPlayer release];
    _audioPlayer = nil;
}

@end
