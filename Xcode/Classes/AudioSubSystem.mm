//
//  AudioSubSystem.m
//  Unity-iPhone
//
//  Created by Joseph Narai on 15/04/13.
//
//

#import "AudioSubSystem.h"

@interface AudioSubSystem ()

@property (nonatomic, retain) NSMutableArray* audioChannels;
@property (nonatomic, retain) NSString* baseURL;
@property (nonatomic) BOOL startedPlaying;

@end

@implementation AudioSubSystem

static AudioSubSystem* _audioSubSystemSingleton = nil;

+(AudioSubSystem*)audioSubSystemSingleton
{
	@synchronized([AudioSubSystem class])
	{
		if (!_audioSubSystemSingleton)
			_audioSubSystemSingleton = [[self alloc] init];
		
		return _audioSubSystemSingleton;
	}
	
	return nil;
}

+ (id)alloc
{
	@synchronized([AudioSubSystem class])
	{
		NSAssert(_audioSubSystemSingleton == nil, @"Attempted to allocate a second instance of a singleton.");
		_audioSubSystemSingleton = [super alloc];
		return _audioSubSystemSingleton;
	}
	
	return nil;
}

-(void)dealloc
{
    [super dealloc];
    [_audioChannels release];
    _audioChannels = nil;
    
    [_baseURL release];
    _baseURL = nil;
    
    [_audioSubSystemSingleton release];
    _audioSubSystemSingleton = nil;
}

- (id) init {
	self = [super init];
	if (self != nil) {
        //_narrationAudioPlayer = //[[NSMutableArray array] retain];
        NSLog(@"AudioSubSystem Initialised");
        
        // Set Audio catetory to work in background
        NSError *sessionError = nil;
        [[AVAudioSession sharedInstance] setDelegate:self];
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:&sessionError];
        
        if( sessionError != nil )
        {
            NSLog(@"sessionError: %@", sessionError);
        }
        
        // Init playlistEntries arrays
        _audioChannels = [[NSMutableArray alloc] initWithObjects: [[AudioChannel alloc] initWithType:AP_Narration andDirectoryName:@"Narration"], [[AudioChannel alloc] initWithType:AP_Music andDirectoryName:@"Music"], [[AudioChannel alloc] initWithType:AP_Effects andDirectoryName:@"Effects"],[[AudioChannel alloc] initWithType:AP_Atmosphere andDirectoryName:@"Atmos"],nil];
        
        // And base URL
        _baseURL = [[NSString alloc] init];
        
        _startedPlaying = NO;
	}
	return self;
}

-(void)playAudioWithChapterIndex:(int)index andTime:(float)time
{
    if( !_startedPlaying )
    {
        _startedPlaying = YES;
    }
    
    AudioChannel* audioChannel = _audioChannels[(int)AP_Narration];
    
    // Assign the current chapter to the chapter index passed through and the current entry to 0
    _currentChapter = index;
    audioChannel.entryIndex = 0;
    
    // Get the current chapter's narration playlist Entries array
    NSArray* chapterPlaylistEntries = audioChannel.playlistEntries[index];
    
    // Set the default found entry to the first entry
    ChapterPlaylistEntry *foundEntry = chapterPlaylistEntries[0];
    
    // Search through the entries, breaking when past the time needed
    for( int i = 0; i < chapterPlaylistEntries.count; i++ )
    {
        ChapterPlaylistEntry *entry = chapterPlaylistEntries[i];
        
        if( [entry time] > time )
        {
            break;
        }
        
        foundEntry = entry;
        audioChannel.entryIndex = i;
    }
    
    [audioChannel playAudioWithEntry:foundEntry nextEntry:nil andTime:time];
    
    [self playAuxAudioWithChapterIndex:index time:time andPlayer:AP_Music];
    [self playAuxAudioWithChapterIndex:index time:time andPlayer:AP_Effects];
}

-(void)playAuxAudioWithChapterIndex:(int)index time:(float)time andPlayer:(int)audioPlayerTypeIndex
{
    // Ensure the audio player type index is valid
    if( audioPlayerTypeIndex >= AP_COUNT )
    {
        NSLog(@"Error: AudioSubSystem.mm : The audio player type index passed into play aux audio isn't valid. Passed in: %d", audioPlayerTypeIndex);
        return;
    }
    
    AudioChannel* audioChannel = _audioChannels[audioPlayerTypeIndex];
    
    // Assign the entries to 0
    audioChannel.entryIndex = 0;
    int nextEntryIndex = 0;
    
    // Get the current chapter's playlist entries array
    NSArray* chapterPlaylistEntries = audioChannel.playlistEntries[index];
    
    // Set the default found entry and next entry to the first entry
    ChapterPlaylistEntry *foundEntry = chapterPlaylistEntries[0];
    ChapterPlaylistEntry *nextEntry = chapterPlaylistEntries[0];
    
    // Search through the entries, breaking when past the time needed
    for( int i = 0; i < chapterPlaylistEntries.count; i++ )
    {
        nextEntryIndex = i;
        nextEntry = chapterPlaylistEntries[i];
        
        if( [nextEntry time] > time )
        {
            break;
        }
        
        foundEntry = nextEntry;
        audioChannel.entryIndex = i;
    }
    
    // If the found entry is the same as the next entry
    if( audioChannel.entryIndex == nextEntryIndex && foundEntry.filename == nextEntry.filename && foundEntry.time == nextEntry.time )
    {
        nextEntry = [audioChannel getNextEntryAndIncrement:NO];
    }
    
    [audioChannel playAudioWithEntry:foundEntry nextEntry:nextEntry andTime:time];
}

-(void)stopAudio
{
    for( int i = 0; i < AP_COUNT; i ++ )
    {
        AudioChannel* channel = _audioChannels[i];
        [channel stopAudio];
    }
}

-(void)loadChapterPlaylistFile:(NSString*)filename forPlayer:(int)audioPlayerTypeIndex
{
    // Ensure Unity has not called play and then is trying to load more files
    if( _startedPlaying )
    {
        NSLog(@"Error: AudioSubSystem.mm : You can not try to load a chapter playlist file after having called play. Fix your calls in Unity.");
        return;
    }
    
    // Ensure the audio player type index is valid
    if( audioPlayerTypeIndex >= AP_COUNT )
    {
        NSLog(@"Error: AudioSubSystem.mm : The audio player type index passed into load chapter playlist file isn't valid. Passed in: %d", audioPlayerTypeIndex);
        return;
    }
    
    // Assign the audio channel to the correct index, and assign a new chapter playlist entries array
    AudioChannel* audioChannel = _audioChannels[audioPlayerTypeIndex];
    NSMutableArray* chapterPlaylistEntries = [NSMutableArray array];
    
    // Convert the filename into a URL
    NSURL* url = [NSURL fileURLWithPath:[NSString stringWithFormat:@"%@%@", _baseURL, filename]];
    
    if( url != nil )
    {
        // Get the contents of the file as a string
        NSString *contents = [NSString stringWithContentsOfURL:url encoding:NSUTF8StringEncoding error:nil];
        
        // Skip the file if it is empty (for a channel with no files in a particular chapter)
        if( contents.length <= 0 )
        {
            NSLog(@"Warning: AudioSubSystem.mm : The file \"%@\" is empty, please ensure that this is intentional. Skipping remainder of file and continuing.", filename);
        }
        else
        {
            // Separate the playlist entries from the contents by new line
            NSArray *playlistEntriesArray = [contents componentsSeparatedByString:@"\n"];
        
            for( int i = 0; i < playlistEntriesArray.count; i++ )
            {
                // Separate the components of each playlist entry by comma
                NSArray *componentsArray = [playlistEntriesArray[i] componentsSeparatedByString:@","];
                
                // Skip the file if it contains formatting errors
                if( componentsArray.count != 2 )
                {
                    NSLog(@"Warning: AudioSubSystem.mm : The file \"%@\" has a formatting error on line %d, please check it. Skipping remainder of file and continuing.", filename, (i+1));
                    break;
                }
                
                // Create a new entry with the components
                ChapterPlaylistEntry *entry = [[ChapterPlaylistEntry alloc] initWithTime:[componentsArray[0] floatValue] andFilename:[NSString stringWithFormat:@"%@%@/%@", _baseURL, audioChannel.directoryName, componentsArray[1]]];
                
                // Add the entry to the playlsitEntries array
                [chapterPlaylistEntries addObject:entry];
            }
        }
        
        // Add the chapter playlist entries to the appropriate player's playlist
        [audioChannel.playlistEntries addObject:chapterPlaylistEntries];
        NSLog(@"Added playlist entries: %@ to audioChannelIndex: %d", chapterPlaylistEntries, audioPlayerTypeIndex);
    }
}

- (NSString*)getCurrentPosition
{
    AudioChannel* narrationAudioChannel = _audioChannels[(int)AP_Narration];
    
    // Return comma deliminated string of current chapter, current entry index and current time through the index file
    NSString *returnString = [NSString stringWithFormat:@"%d,%d,%f",_currentChapter,narrationAudioChannel.entryIndex,[narrationAudioChannel.audioPlayer currentTime]];
    return returnString;
}

-(void)setBaseURL:(NSString *)baseURL
{
    _baseURL = baseURL;
}

@end

// Converts C style string to NSString
NSString* CreateNSString (const char* string)
{
	if (string)
		return [NSString stringWithUTF8String: string];
	else
		return [NSString stringWithUTF8String: ""];
}

// Helper method to create C string copy
char* MakeStringCopy (const char* string)
{
	if (string == NULL)
		return NULL;
	
	char* res = (char*)malloc(strlen(string) + 1);
	strcpy(res, string);
	return res;
}

extern "C"
{
    void SetBaseURL( const char* baseURL )
    {
        [[AudioSubSystem audioSubSystemSingleton] setBaseURL:CreateNSString(baseURL)];
    }
    
    void PlayAudio( int chapterIndex, float time )
    {
        [[AudioSubSystem audioSubSystemSingleton] playAudioWithChapterIndex:chapterIndex andTime:time];
    }
    
    void LoadChapterPlaylistFile( const char* fileName, int audioPlayerTypeIndex )
    {
        [[AudioSubSystem audioSubSystemSingleton] loadChapterPlaylistFile:CreateNSString(fileName) forPlayer:(int)audioPlayerTypeIndex];
    }
    
    const char* GetCurrentPosition()
    {
        return MakeStringCopy( [[[AudioSubSystem audioSubSystemSingleton] getCurrentPosition] UTF8String] );
    }
}
