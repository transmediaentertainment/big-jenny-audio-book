//
//  ChapterPlaylistEntry.h
//  Unity-iPhone
//
//  Created by Simon on 24/04/13.
//
//

#import <Foundation/Foundation.h>

@interface ChapterPlaylistEntry : NSObject

- (id)initWithTime:(float)time andFilename: (NSString*)filename;

@property (nonatomic) float time;
@property (nonatomic, retain) NSString* filename;

@end
