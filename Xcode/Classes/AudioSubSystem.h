//
//  AudioSubSystem.h
//  Unity-iPhone
//
//  Created by Joseph Narai on 15/04/13.
//
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVAudioPlayer.h>
#import <AVFoundation/AVFoundation.h>
#import "AudioChannel.h"
#import "ChapterPlaylistEntry.h"

@interface AudioSubSystem : NSObject

+ (AudioSubSystem*)audioSubSystemSingleton;

-(void)playAuxAudioWithChapterIndex:(int)index time:(float)time andPlayer:(int)audioPlayerTypeIndex;
-(void)stopAudio;

@property (nonatomic) int currentChapter;

@end
